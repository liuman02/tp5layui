/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : localhost:3306
 Source Schema         : tp5layui

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 21/09/2019 09:41:20
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sysmenu
-- ----------------------------
DROP TABLE IF EXISTS `sysmenu`;
CREATE TABLE `sysmenu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `name` char(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `type` tinyint(1) NOT NULL DEFAULT 1,
  `pid` int(11) NOT NULL,
  `ismenu` tinyint(1) NOT NULL,
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `sort` tinyint(11) NOT NULL,
  `condition` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 255 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sysmenu
-- ----------------------------
INSERT INTO `sysmenu` VALUES (1, '后台首页', 'admin/index/index', 1, 5, 0, 'layui-icon-loading', 1, 0, '');
INSERT INTO `sysmenu` VALUES (2, '中间', 'Layout/center', 1, 1, 0, '阿斯蒂芬', 1, 2, 'SD敢达阿斯蒂芬');
INSERT INTO `sysmenu` VALUES (3, '左边', 'Layout/west', 1, 1, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (4, '上边', 'Layout/north', 1, 1, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (5, '系统管理', '系统管理', 1, 0, 1, 'layui-icon-set-sm', 1, 10, '上的风格是大法官');
INSERT INTO `sysmenu` VALUES (6, '用户管理', 'admin/sysuser/index', 2, 5, 1, 'layui-icon-username', 1, 0, '');
INSERT INTO `sysmenu` VALUES (7, '用户添加', 'admin/sysuser/add', 2, 6, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (8, '菜单管理', 'admin/sysmenu/index', 1, 5, 1, 'layui-icon-template-1', 1, 0, '');
INSERT INTO `sysmenu` VALUES (9, '角色管理', 'admin/sysrole/index', 1, 5, 1, 'layui-icon-user', 1, 0, '');
INSERT INTO `sysmenu` VALUES (10, '节点获取所有', 'admin/sysmenu/view', 1, 8, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (11, '节点编辑', 'admin/sysmenu/edit', 1, 8, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (12, '节点添加', 'admin/sysmenu/add', 1, 8, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (13, '节点删除', 'admin/sysmenu/delete', 1, 8, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (14, '用户获取所有', 'admin/sysuser/view', 1, 6, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (15, '用户编辑', 'admin/sysuser/edit', 1, 6, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (16, '用户删除', 'admin/sysuser/delete', 1, 6, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (17, '用户公共组件', 'User/viewWidget', 1, 174, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (18, '角色获取所有', 'admin/sysrole/view', 1, 9, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (19, '角色添加', 'admin/sysrole/add', 1, 9, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (20, '角色编辑', 'admin/sysrole/edit', 1, 9, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (21, '角色删除', 'admin/sysrole/delete', 1, 9, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (22, '工资级别管理', 'Wagegrade/index', 1, 174, 1, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (23, '工资级别获取所有', 'Wagegrade/view', 1, 22, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (24, '工资级别添加', 'Wagegrade/add', 1, 22, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (25, '工资级别编辑', 'Wagegrade/edit', 1, 22, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (26, '工资级别删除', 'Wagegrade/delete', 1, 22, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (27, '服务器获取by服务商id组件', 'Fuwuqi/viewWidgetByFuwushangid', 1, 22, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (28, '手机卡管理', 'Zctelcard/index', 1, 172, 1, '', 1, 12, '');
INSERT INTO `sysmenu` VALUES (29, '手机卡获取所有', 'Zctelcard/view', 1, 28, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (30, '手机卡添加', 'Zctelcard/add', 1, 28, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (31, '手机卡编辑', 'Zctelcard/edit', 1, 28, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (32, '手机卡删除', 'Zctelcard/delete', 1, 28, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (33, '服务商获取组件', 'Fuwushang/viewWidget', 1, 28, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (34, '服务商byid获取组件', 'Fuwushang/viewWidgetById', 1, 28, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (35, '公告管理', 'Gonggao/index', 1, 171, 1, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (36, '公告获取所有', 'Gonggao/view', 1, 35, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (37, '公告添加', 'Gonggao/add', 1, 35, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (38, '公告编辑', 'Gonggao/edit', 1, 35, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (39, '公告删除', 'Gonggao/delete', 1, 35, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (40, '系统信息', 'Layout/main', 1, 1, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (41, '图表管理', 'Pic/index', 1, 5, 0, 'layui-icon-loading', 1, 0, '');
INSERT INTO `sysmenu` VALUES (42, '图表high', 'Pic/high', 1, 41, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (43, '图表pic', 'Pic/pic', 1, 41, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (44, '评论管理', 'Pinglun/index', 1, 5, 0, 'layui-icon-loading', 1, 0, '');
INSERT INTO `sysmenu` VALUES (45, '评论获取所有', 'Pinglun/view', 1, 44, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (46, '评论添加', 'Pinglun/add', 1, 44, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (47, '评论编辑', 'Pinglun/edit', 1, 44, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (48, '评论删除', 'Pinglun/delete', 1, 44, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (49, '评论审核', 'Pinglun/shenhe', 1, 44, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (50, '评论禁用', 'Pinglun/jinyong', 1, 44, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (51, '项目管理', 'Project/index', 1, 5, 1, 'layui-icon-list', 1, 0, '');
INSERT INTO `sysmenu` VALUES (52, '项目添加', 'Project/add', 1, 51, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (53, '项目编辑', 'Project/edit', 1, 51, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (54, '项目删除', 'Project/delete', 1, 51, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (55, '项目根据登录人员获取项目信息组件', 'Project/viewWidget', 1, 51, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (56, 'QQ管理', 'Qq/index', 1, 174, 1, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (57, 'QQ获取所有', 'Qq/view', 1, 56, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (58, 'QQ添加', 'Qq/add', 1, 56, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (59, 'QQ编辑', 'Qq/edit', 1, 56, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (60, 'QQ删除', 'Qq/delete', 1, 56, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (61, '账号粉丝管理', 'Zczhanghaofensi/index', 1, 172, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (62, '账号粉丝获取所有', 'Zczhanghaofensi/view', 1, 61, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (63, '账号粉丝添加', 'Zczhanghaofensi/add', 1, 61, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (64, '账号粉丝编辑', 'Zczhanghaofensi/edit', 1, 61, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (65, '账号粉丝删除', 'Zczhanghaofensi/delete', 1, 61, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (66, '手机管理', 'Zctel/index', 1, 172, 1, '', 1, 11, '');
INSERT INTO `sysmenu` VALUES (67, '手机管理获取所有', 'Zctel/view', 1, 66, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (68, '手机管理添加', 'Zctel/add', 1, 66, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (69, '手机管理编辑', 'Zctel/edit', 1, 66, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (70, '手机管理删除', 'Zctel/delete', 1, 66, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (72, '竞价账号管理', 'Zcjingjianumber/index', 1, 172, 1, '', 1, 16, '');
INSERT INTO `sysmenu` VALUES (73, '竞价账号获取所有', 'Zcjingjianumber/view', 1, 72, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (74, '竞价账号添加', 'Zcjingjianumber/add', 1, 72, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (75, '竞价账号编辑', 'Zcjingjianumber/edit', 1, 72, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (76, '竞价账号删除', 'Zcjingjianumber/delete', 1, 72, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (77, '网站加速管理', 'Webjiasu/index', 1, 174, 1, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (78, '网站加速获取所有', 'Webjiasu/view', 1, 77, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (79, '网站加速添加', 'Webjiasu/add', 1, 77, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (80, '网站加速编辑', 'Webjiasu/edit', 1, 77, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (81, '网站加速删除', 'Webjiasu/delete', 1, 77, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (82, '办公室管理', 'Zcoffice/index', 1, 172, 1, '', 1, 14, '');
INSERT INTO `sysmenu` VALUES (83, '办公室管理获取所有', 'Zcoffice/view', 1, 82, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (84, '办公室管理添加', 'Zcoffice/add', 1, 82, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (85, '办公室管理编辑', 'Zcoffice/edit', 1, 82, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (86, '办公室管理删除', 'Zcoffice/delete', 1, 82, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (87, '域名管理', 'Zcdomain/index', 1, 172, 1, '', 1, 15, '');
INSERT INTO `sysmenu` VALUES (88, '域名获取所有', 'Zcdomain/view', 1, 87, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (89, '域名添加', 'Zcdomain/add', 1, 87, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (90, '域名编辑', 'Zcdomain/edit', 1, 87, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (91, '域名删除', 'Zcdomain/delete', 1, 87, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (96, '服务器维护', '服务器维护', 1, 174, 0, '', 1, 2, '');
INSERT INTO `sysmenu` VALUES (97, '账号粉丝数据', 'Zczhanghao/fensi', 1, 181, 0, '', 1, 0, '');
INSERT INTO `sysmenu` VALUES (98, '其他工具', '其他工具', 1, 174, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (99, '后台portal', 'Layout/portal', 1, 1, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (100, '后台首页公告', 'Layout/gonggao', 1, 1, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (101, '后勤信息', '后勤信息', 1, 0, 1, 'layui-icon-read', 1, 2, '');
INSERT INTO `sysmenu` VALUES (102, '床位管理', 'Chuangwei/index', 1, 101, 1, '', 1, 9, '');
INSERT INTO `sysmenu` VALUES (103, '床位获取所有', 'Chuangwei/view', 1, 102, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (104, '床位添加', 'Chuangwei/add', 1, 102, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (105, '床位编辑', 'Chuangwei/edit', 1, 102, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (106, '床位删除', 'Chuangwei/delete', 1, 102, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (107, '床位公共组件', 'Chuangwei/viewWidget', 1, 102, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (108, '床位根据宿舍id获取公共组件', 'Chuangwei/viewWidgetSushe', 1, 102, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (109, '床位导出报表', 'Chuangwei/daochuexcel', 1, 102, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (110, '部门管理', 'Dept/index', 1, 5, 1, 'layui-icon-group', 1, 1, '');
INSERT INTO `sysmenu` VALUES (111, '部门获取所有', 'Dept/view', 1, 110, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (112, '部门添加', 'Dept/add', 1, 110, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (113, '部门编辑', 'Dept/edit', 1, 110, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (114, '部门删除', 'Dept/delete', 1, 110, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (115, '用户管理重置密码', 'admin/sysuser/reset', 1, 6, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (116, '人员管理', 'Employee/index', 1, 171, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (117, '人员获取所有', 'Employee/view', 1, 116, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (118, '人员添加', 'Employee/add', 1, 116, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (119, '人员编辑', 'Employee/edit', 1, 116, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (120, '人员删除', 'Employee/delete', 1, 116, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (121, '人员公共组件', 'Employee/viewWidget', 1, 116, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (122, '人员导出报表', 'Employee/daochuexcel', 1, 116, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (123, '人员获取籍贯', 'Employee/seachJiguan', 1, 116, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (124, '人员离职管理', 'Employeelizhi/index', 1, 171, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (125, '人员离职获取所有', 'Employeelizhi/view', 1, 124, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (126, '人员离职编辑', 'Employeelizhi/edit', 1, 124, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (127, '人员离职删除', 'Employeelizhi/delete', 1, 124, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (128, '岗位管理', 'Gangwei/index', 1, 174, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (129, '岗位获取所有', 'Gangwei/view', 1, 128, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (130, '岗位添加', 'Gangwei/add', 1, 128, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (131, '岗位编辑', 'Gangwei/edit', 1, 128, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (132, '岗位删除', 'Gangwei/delete', 1, 128, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (133, '岗位功能组件', 'Gangwei/viewWidget', 1, 128, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (134, '户型管理', 'Huxing/index', 1, 101, 1, '', 1, 10, '');
INSERT INTO `sysmenu` VALUES (135, '户型获取所有', 'Huxing/view', 1, 134, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (136, '户型添加', 'Huxing/add', 1, 134, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (137, '户型编辑', 'Huxing/edit', 1, 134, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (138, '户型删除', 'Huxing/delete', 1, 134, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (139, '户型公共组件', 'Huxing/viewWidget', 1, 134, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (140, '户型获取by宿舍id公共组件', 'Huxing/viewWidgetSushe', 1, 134, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (141, '楼层管理', 'Louceng/index', 1, 174, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (142, '楼层获取所有', 'Louceng/view', 1, 141, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (143, '楼层添加', 'Louceng/add', 1, 141, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (144, '楼层编辑', 'Louceng/edit', 1, 141, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (145, '楼层删除', 'Louceng/delete', 1, 141, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (146, '楼层公共组件', 'Louceng/viewWidget', 1, 141, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (147, '电脑配置管理', 'Peizhi/index', 1, 174, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (148, '配置获取所有', 'Peizhi/view', 1, 147, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (149, '配置添加', 'Peizhi/add', 1, 147, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (150, '配置编辑', 'Peizhi/edit', 1, 147, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (151, '配置删除', 'Peizhi/delete', 1, 147, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (152, '配置公共组件', 'Peizhi/viewWidget', 1, 147, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (153, '电脑设备管理', 'Shebei/index', 1, 172, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (154, '设备添加', 'Shebei/add', 1, 153, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (155, '设备编辑', 'Shebei/edit', 1, 153, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (156, '设备删除', 'Shebei/delete', 1, 153, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (157, '设备获取所有', 'Shebei/view', 1, 153, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (158, '设备公共组件', 'Shebei/viewWidget', 1, 153, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (159, '设备by设备id公共组件', 'Shebei/viewWidgetShebei', 1, 153, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (160, '设备by用户id公共组件', 'Shebei/viewWidgetEmployeeid', 1, 153, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (161, '设备导出报表', 'Shebei/daochuexcel', 1, 153, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (162, '电脑设备使用情况', 'Shebeishiyong/index', 1, 172, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (163, '设备使用获取所有', 'Shebeishiyong/view', 1, 162, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (164, '设备使用编辑', 'Shebeishiyong/edit', 1, 162, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (165, '宿舍管理', 'Sushe/index', 1, 101, 1, '', 1, 7, '');
INSERT INTO `sysmenu` VALUES (166, '宿舍获取所有', 'Sushe/view', 1, 165, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (167, '宿舍添加', 'Sushe/add', 1, 165, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (168, '宿舍编辑', 'Sushe/edit', 1, 165, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (169, '宿舍删除', 'Sushe/delete', 1, 165, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (170, '宿舍公共组件', 'Sushe/viewWidget', 1, 165, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (171, '人事信息', '人事信息', 1, 0, 1, 'layui-icon-username', 1, 1, '');
INSERT INTO `sysmenu` VALUES (172, '资产管理', '资产管理', 1, 0, 1, 'layui-icon-rmb', 1, 3, '');
INSERT INTO `sysmenu` VALUES (174, '基础信息', '基础信息', 1, 0, 1, 'layui-icon-component', 1, 9, '');
INSERT INTO `sysmenu` VALUES (175, '项目获取所有', 'Project/view', 1, 51, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (176, '系统日志', 'Log/index', 1, 5, 1, 'layui-icon-log', 1, 8, '');
INSERT INTO `sysmenu` VALUES (177, '系统日志批量删除', 'Log/delete', 1, 176, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (178, '系统日志获取所有', 'Log/view', 1, 176, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (179, '在线用户', 'Useronline/index', 1, 5, 1, 'layui-icon-friends', 1, 9, '');
INSERT INTO `sysmenu` VALUES (180, '在线用户获取所有', 'Useronline/view', 1, 179, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (181, '账号管理', 'Zczhanghao/index', 1, 172, 1, '', 1, 10, '');
INSERT INTO `sysmenu` VALUES (182, '账号管理获取所有', 'Zczhanghao/view', 1, 181, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (183, '账号管理添加', 'Zczhanghao/add', 1, 181, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (184, '账号管理编辑', 'Zczhanghao/edit', 1, 181, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (185, '账号管理删除', 'Zczhanghao/delete', 1, 181, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (186, '打印机管理删除', 'Zcprinter/delete', 1, 190, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (187, '打印机管理添加', 'Zcprinter/add', 1, 190, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (188, '打印机管理编辑', 'Zcprinter/edit', 1, 190, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (189, '打印机管理获取所有', 'Zcprinter/view', 1, 190, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (190, '打印机管理', 'Zcprinter/index', 1, 172, 1, '', 1, 13, '');
INSERT INTO `sysmenu` VALUES (191, '服务器管理', 'Zcfuwuqi/index', 1, 172, 1, '', 1, 17, '');
INSERT INTO `sysmenu` VALUES (192, '服务器管理获取所有', 'Zcfuwuqi/view', 1, 191, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (193, '服务器管理添加', 'Zcfuwuqi/add', 1, 191, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (194, '服务器管理删除', 'Zcfuwuqi/delete', 1, 191, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (195, '服务器管理编辑', 'Zcfuwuqi/edit', 1, 191, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (196, '菜品管理', 'Bccandan/index', 1, 101, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (197, '报餐管理', 'Baocan/index', 1, 101, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (198, '菜品管理获取所有', 'Bccandan/view', 1, 196, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (199, '菜品管理添加', 'Bccandan/add', 1, 196, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (200, '菜品管理编辑', 'Bccandan/edit', 1, 196, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (201, '菜品管理删除', 'Bccandan/delete', 1, 196, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (202, '报餐管理获取所有', 'Baocan/view', 1, 197, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (203, '报餐管理导出', 'Baocan/daochuexcel', 1, 197, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (204, '通知管理', 'Bcnotify/index', 1, 101, 1, '', 1, 5, '');
INSERT INTO `sysmenu` VALUES (205, '通知管理获取所有', 'Bcnotify/view', 1, 204, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (206, '通知管理添加', 'Bcnotify/add', 1, 204, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (207, '通知管理编辑', 'Bcnotify/edit', 1, 204, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (208, '通知管理删除', 'Bcnotify/delete', 1, 204, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (209, '报餐管理编辑', 'Baocan/edit', 1, 197, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (210, '问题管理', 'Quiz/index', 1, 174, 1, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (211, '问题管理-获取所有', 'Quiz/view', 1, 210, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (212, '问题管理-添加', 'Quiz/add', 1, 210, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (213, '问题管理-编辑', 'Quiz/edit', 1, 210, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (214, '问题管理-删除', 'Quiz/delete', 1, 210, 0, '', 1, 1, '');
INSERT INTO `sysmenu` VALUES (254, '是大法官是大法官', '上的风格是大法官', 1, 1, 1, '', 1, 1, '是大法官是大法官');

-- ----------------------------
-- Table structure for sysrole
-- ----------------------------
DROP TABLE IF EXISTS `sysrole`;
CREATE TABLE `sysrole`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `rules` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sysrole
-- ----------------------------
INSERT INTO `sysrole` VALUES (1, '行政组', 1, '0', 'dgh');
INSERT INTO `sysrole` VALUES (2, '人事组', 1, '0', 'rty');
INSERT INTO `sysrole` VALUES (5, '网管组df', 1, '0', 'sdfg');
INSERT INTO `sysrole` VALUES (6, '财务组', 1, '0', 'sdfg ');
INSERT INTO `sysrole` VALUES (7, 'erty 问问', 1, NULL, 'erty ');
INSERT INTO `sysrole` VALUES (10, '规划局', 1, NULL, '飞规划局');
INSERT INTO `sysrole` VALUES (11, '儿童医院', 1, NULL, '儿童医院');
INSERT INTO `sysrole` VALUES (12, '认同与', 1, NULL, '认同与');
INSERT INTO `sysrole` VALUES (13, '阿斯蒂芬', 1, NULL, NULL);
INSERT INTO `sysrole` VALUES (14, '大发光火', 1, NULL, NULL);
INSERT INTO `sysrole` VALUES (15, '儿童医院', 1, NULL, NULL);
INSERT INTO `sysrole` VALUES (16, '儿童医院', 1, NULL, NULL);
INSERT INTO `sysrole` VALUES (17, '儿童医院', 1, NULL, NULL);

-- ----------------------------
-- Table structure for sysrole_menu
-- ----------------------------
DROP TABLE IF EXISTS `sysrole_menu`;
CREATE TABLE `sysrole_menu`  (
  `role_id` smallint(6) UNSIGNED NOT NULL,
  `menu_id` smallint(6) UNSIGNED NOT NULL,
  `level` tinyint(1) NOT NULL DEFAULT 0,
  `pid` smallint(6) NOT NULL DEFAULT 0,
  `module` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  INDEX `groupId`(`role_id`) USING BTREE,
  INDEX `nodeId`(`menu_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sysrole_menu
-- ----------------------------
INSERT INTO `sysrole_menu` VALUES (138, 97, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 2, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 142, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 143, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 144, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 145, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 146, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 6, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 85, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 86, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 87, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 88, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 89, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 90, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 91, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 92, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 203, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 7, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 150, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 151, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 152, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 153, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 154, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 30, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 168, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 169, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 173, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 143, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 144, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 145, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 146, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 6, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 85, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 86, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 87, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 88, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 89, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 90, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 91, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 92, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 203, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 7, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 150, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 151, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 152, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 153, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 154, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 30, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 168, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 169, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 173, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 174, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 175, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 188, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 84, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 149, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 155, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 158, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 159, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 160, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 165, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 148, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 157, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 156, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 163, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 164, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 187, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 161, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 182, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 183, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 184, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 185, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 186, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 192, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 170, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 198, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 199, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 174, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 175, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 7, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 150, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 151, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 152, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 188, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 84, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 149, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 155, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 158, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 159, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 200, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 201, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 152, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 151, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 150, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 149, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 148, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 143, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 142, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 188, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 187, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 164, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 163, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 161, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 160, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 159, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 158, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 157, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 156, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 155, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 154, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 153, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 84, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 30, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 172, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 203, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 199, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 198, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 165, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 202, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 160, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 165, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 148, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 157, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 156, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 163, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 164, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 187, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 161, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 182, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 183, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 184, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 185, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 186, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 192, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 170, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 198, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 199, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 200, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 201, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 202, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 172, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 7, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 150, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 151, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 152, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 153, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 154, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 30, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 188, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 84, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 149, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 155, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 158, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 159, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 160, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 165, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 148, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 157, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 156, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 163, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 164, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 187, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 161, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 182, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 183, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 184, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 185, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 186, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 192, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 170, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 198, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 199, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 172, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 177, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 178, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 179, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 180, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 181, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 176, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 190, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 191, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 193, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 189, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 195, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 196, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 197, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 194, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 205, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 206, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 172, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 177, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 178, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 179, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 180, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 181, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 176, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 190, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 191, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 193, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 189, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 195, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 196, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 197, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 194, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 205, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 206, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 207, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 204, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 153, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 154, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 30, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 188, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 84, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 149, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 155, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 158, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 159, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 160, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 165, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 148, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 157, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 156, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 163, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 164, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 187, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 161, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 198, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 199, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 200, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 201, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 202, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 172, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 205, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 206, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 177, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 178, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 179, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 180, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 181, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 176, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 190, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 191, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 193, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 189, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 195, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 196, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 197, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 194, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 205, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 206, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 207, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 204, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 7, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 2, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 207, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 204, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 207, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 204, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 96, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 95, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 94, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 93, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 203, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 2, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 94, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 93, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 203, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 2, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 94, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 93, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 142, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 2, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 94, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 93, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 2, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 151, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 94, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 92, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 7, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 150, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 151, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 152, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 153, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 154, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 30, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 169, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 188, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 84, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 149, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 155, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 158, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 159, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 160, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 165, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 288, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 148, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 157, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 156, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 275, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 276, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 208, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 260, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 261, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 209, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 210, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 247, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 248, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 211, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 270, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 212, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 266, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 213, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 240, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 241, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 242, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 243, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 244, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 245, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 143, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 6, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 92, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 7, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 150, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 151, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 152, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 153, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 154, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 30, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 168, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 169, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 173, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 174, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 175, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 188, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 84, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 149, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 155, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 158, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 159, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 160, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 165, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 288, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 148, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 157, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 156, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 163, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 164, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 187, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 161, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 198, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 199, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 200, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 201, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 202, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 172, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 274, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 275, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 276, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 277, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 278, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 279, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 208, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 259, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 260, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 261, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 262, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 263, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 264, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 209, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 252, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 253, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 254, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 255, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 256, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 257, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 258, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 210, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 246, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 247, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 248, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 249, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 250, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 251, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 211, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 269, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 270, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 271, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 272, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 273, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 289, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 290, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 212, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 265, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 266, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 267, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 268, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 213, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 280, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 281, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 282, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 283, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 284, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 285, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 286, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 287, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 214, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 219, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 215, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 231, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 232, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 233, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 234, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 235, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 236, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 237, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 238, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 239, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 216, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 225, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 220, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 221, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 222, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 223, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 224, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 218, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 226, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 227, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 228, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 152, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 153, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 154, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 30, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 149, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 155, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 158, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 159, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 160, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 165, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 288, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 148, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 157, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 156, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 275, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 260, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 253, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 247, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 270, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 212, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 266, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 240, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 241, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 242, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 215, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 231, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 232, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 236, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 237, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 238, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 216, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 225, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 229, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 230, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 226, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 227, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 228, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 229, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 230, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 217, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 217, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 150, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (5, 42, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (1, 216, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (1, 215, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (1, 100, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (1, 99, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (1, 40, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (1, 4, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (1, 3, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (1, 2, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (1, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (8, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (8, 2, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (8, 3, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (8, 4, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (8, 40, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (8, 99, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (8, 100, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (8, 215, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (8, 216, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (5, 43, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (9, 7, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 5, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 2, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 3, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 4, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 40, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 99, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 100, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 215, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 216, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 8, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 10, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 11, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 12, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 13, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 9, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 18, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 19, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 20, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 21, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 41, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 42, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 43, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 44, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 45, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 46, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 47, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 48, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 49, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 50, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 51, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 52, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 53, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 54, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 55, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 175, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 110, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 111, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 112, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 113, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 114, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 115, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 176, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 177, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 178, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 179, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 180, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (11, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (11, 2, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (11, 3, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (11, 4, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (11, 40, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (11, 99, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (11, 100, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (11, 215, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (11, 216, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (12, 2, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (2, 5, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (2, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (2, 6, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (2, 14, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (2, 8, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (2, 10, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (2, 9, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (2, 18, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 6, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 7, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 14, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 15, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 16, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 17, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (15, 5, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (15, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (16, 5, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (16, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (17, 5, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (17, 1, 0, 0, NULL);

-- ----------------------------
-- Table structure for sysrole_user
-- ----------------------------
DROP TABLE IF EXISTS `sysrole_user`;
CREATE TABLE `sysrole_user`  (
  `user_id` mediumint(8) UNSIGNED NOT NULL,
  `role_id` mediumint(8) UNSIGNED NOT NULL,
  UNIQUE INDEX `uid_group_id`(`user_id`, `role_id`) USING BTREE,
  INDEX `uid`(`user_id`) USING BTREE,
  INDEX `group_id`(`role_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sysrole_user
-- ----------------------------
INSERT INTO `sysrole_user` VALUES (139, 2);
INSERT INTO `sysrole_user` VALUES (148, 1);
INSERT INTO `sysrole_user` VALUES (149, 2);
INSERT INTO `sysrole_user` VALUES (152, 2);
INSERT INTO `sysrole_user` VALUES (155, 1);
INSERT INTO `sysrole_user` VALUES (158, 2);
INSERT INTO `sysrole_user` VALUES (164, 2);
INSERT INTO `sysrole_user` VALUES (166, 0);
INSERT INTO `sysrole_user` VALUES (167, 2);
INSERT INTO `sysrole_user` VALUES (168, 5);
INSERT INTO `sysrole_user` VALUES (169, 1);
INSERT INTO `sysrole_user` VALUES (170, 1);
INSERT INTO `sysrole_user` VALUES (173, 5);
INSERT INTO `sysrole_user` VALUES (173, 6);
INSERT INTO `sysrole_user` VALUES (175, 1);
INSERT INTO `sysrole_user` VALUES (175, 2);
INSERT INTO `sysrole_user` VALUES (176, 1);
INSERT INTO `sysrole_user` VALUES (177, 1);
INSERT INTO `sysrole_user` VALUES (181, 2);
INSERT INTO `sysrole_user` VALUES (185, 2);
INSERT INTO `sysrole_user` VALUES (191, 2);
INSERT INTO `sysrole_user` VALUES (192, 2);
INSERT INTO `sysrole_user` VALUES (194, 2);

-- ----------------------------
-- Table structure for sysuser
-- ----------------------------
DROP TABLE IF EXISTS `sysuser`;
CREATE TABLE `sysuser`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nickname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `bind_account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `last_login_time` int(11) UNSIGNED DEFAULT 0,
  `last_login_ip` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `login_count` mediumint(8) UNSIGNED DEFAULT 0,
  `verify` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `sex` tinyint(2) DEFAULT NULL,
  `mobile` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `email` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` datetime(0) DEFAULT NULL,
  `update_time` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `status` tinyint(1) DEFAULT 0,
  `type_id` tinyint(2) UNSIGNED DEFAULT 0,
  `info` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `projectid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `deptid` int(11) DEFAULT NULL,
  `chuangweiid` int(11) DEFAULT NULL,
  `susheid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `account`(`account`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 177 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sysuser
-- ----------------------------
INSERT INTO `sysuser` VALUES (139, 'admin', 'admin', 'e10adc3949ba59abbe56e057f20f883e', '', 1566778366, '127.0.0.1', 840, NULL, 1, '15643132', '222@qq.com', '沃尔特沃尔特', NULL, '2019-08-29 17:43:05', 1, 0, '', '28,15,17,8,22,18,11,14,23,7,26,2', NULL, NULL, NULL);
INSERT INTO `sysuser` VALUES (166, 'che', 'che', 'b36707fc0db96f09043744a782f1737d', '', 1434508189, '222.90.140.101', 56, NULL, 1, '15643132', '445512@qq.com', '', NULL, '2019-08-28 14:59:04', 1, 0, '', '83', NULL, NULL, NULL);
INSERT INTO `sysuser` VALUES (167, '李丽美', '李丽美', 'e10adc3949ba59abbe56e057f20f883e', '', 1501125438, '1.80.1.109', 110, NULL, 0, '15643132', '45663@qq.com', '', NULL, '2019-08-28 14:58:53', 1, 0, '', '84', NULL, NULL, NULL);
INSERT INTO `sysuser` VALUES (168, '高翔', '高翔', 'e10adc3949ba59abbe56e057f20f883e', '', 1501828892, '113.140.251.109', 16, NULL, 0, '15643132', 'fffff@qq.com', '', NULL, '2019-08-28 14:58:53', 1, 0, '', '77,74,73,72,71,56,55,54,53', NULL, NULL, NULL);
INSERT INTO `sysuser` VALUES (169, '贺宝霞', '贺宝霞', 'e10adc3949ba59abbe56e057f20f883e', '', 1505273712, '36.47.160.38', 149, NULL, 0, '15643132', '111@qq.com', '', NULL, '2019-08-28 14:58:53', 1, 0, '', '83', NULL, NULL, NULL);
INSERT INTO `sysuser` VALUES (170, '正小娟', '正小娟', 'e10adc3949ba59abbe56e057f20f883e', '', 1507951452, '1.80.3.254', 770, NULL, 0, '15643132', '122@qq.com', '', NULL, '2019-08-28 14:58:53', 1, 0, '', '83', NULL, NULL, NULL);
INSERT INTO `sysuser` VALUES (173, '额大热天', '沃尔特', 'e10adc3949ba59abbe56e057f20f883e', '额大热天', 0, NULL, 0, NULL, 1, '儿童', '沃尔特', '沃尔特', NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysuser` VALUES (175, '额大热天人', '沃尔特', 'e10adc3949ba59abbe56e057f20f883e', '额大热天人', 0, NULL, 0, NULL, 1, '儿童', '沃尔特', '沃尔特', NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysuser` VALUES (176, '是大', '是', 'e10adc3949ba59abbe56e057f20f883e', '是大法官', 0, NULL, 0, NULL, 0, '是', '', '是大法官是大法官', NULL, '2019-08-28 17:15:43', 0, 0, NULL, NULL, NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
