<?php

namespace app\common\exception;

use think\exception\Handle;
use think\exception\ValidateException;
use think\exception\HttpException;
use think\exception\PDOException;
use think\exception\ErrorException;

class ExceptionMy extends Handle
{
    public function render1(Exception $e)
    {
        $this->error($e->getMessage());
        // 参数验证错误
        if ($e instanceof ValidateException) {
            return json($e->getError(), 422);
        }

        // 参数验证错误
        if ($e instanceof PDOException) {
            dump($e->getMessage());
            die;
            return json($e->getError(), 500);
        }

        if ($e instanceof ErrorException) {
            //dump($e->getMessage());
            //die;
            return json($e->getError(), 400);
        }
        // 请求异常
        if ($e instanceof HttpException && request()->isAjax()) {
            return response($e->getMessage(), $e->getStatusCode());
        }

        // 其他错误交给系统处理
        //return parent::render($e);
    }

}