<?php

namespace app\admin\model;

use think\Model;

class SysroleModel extends Model
{
    public function menus()
    {
        return $this->belongsToMany('SysmenuModel','Sysrole_menu','menu_id','role_id');
    }
}