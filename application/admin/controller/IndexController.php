<?php

namespace app\admin\controller;


use think\facade\View;
use think\Db;
use Auth;
use app\common\controller\AdminBaseController;

class IndexController extends AdminBaseController
{
    public function index()
    {
        //dump(session("sysuser.sysmenu"));
        //dump($user);
        return $this->fetch();
    }

    public function hello($name = 'ThinkPHP5')
    {
        return 'admin->,' . $name;
    }
}
